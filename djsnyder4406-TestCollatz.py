#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

        s = "150 250\n"
        x, y = collatz_read(s)
        self.assertEqual(x, 150)
        self.assertEqual(y, 250)

    def test_one_int(self):
        string = "3"
        self.assertRaises(IndexError, collatz_read, string)

    def test_two_int(self):
        string = " 5 6 7 8"
        a, b = collatz_read(string)
        self.assertEqual(a, 5)
        self.assertEqual(b, 6)

    def test_neg_int(self):
        string = "-2 -4"
        i, j = collatz_read(string)
        self.assertEqual(i, -2)
        self.assertEqual(j, -4)

    def test_null(self):
        string = ""
        self.assertRaises(IndexError, collatz_read, string)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_same(self):
        case = collatz_eval(7, 7)
        self.assertEqual(case, 17)
        case = collatz_eval(9, 9)
        self.assertEqual(case, 20)
        case = collatz_eval(6, 6)
        self.assertEqual(case, 9)

    def test_negative(self):
        self.assertRaises(AssertionError, collatz_eval, -3, -4)
        self.assertRaises(AssertionError, collatz_eval, 3, -4)
        self.assertRaises(AssertionError, collatz_eval, -3, 4)

    def test_rev(self):
        case = collatz_eval(10, 1)
        self.assertEqual(case, 20)
        case = collatz_eval(200, 100)
        self.assertEqual(case, 125)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_check(self):
        w = StringIO()
        collatz_print(w, 1000, 1000, 112)
        self.assertEqual(w.getvalue(), "1000 1000 112\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_check(self):
        r = StringIO("4 658\n89 841\n20 324\n15 61\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "4 658 145\n89 841 171\n20 324 131\n15 61 113\n")

    def test_solve_null(self):
        r = None
        w = None
        self.assertRaises(TypeError, collatz_solve, r, w)

    def test_empty(self):
        r = StringIO()
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
